(require 'package)

(setq package-archives '(("gnu" . "https://elpa.gnu.org/packages/")
                         ("marmalade" . "https://marmalade-repo.org/packages/")
                         ("melpa" . "https://melpa.org/packages/")))

(add-to-list 'load-path "~/.emacs.d/lisp/")

;; Not need as of 26.1
;(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;(require 'use-package)
(eval-when-compile (require 'use-package))


;; Find programmer i ~/bin/emacs
(setq exec-path (append exec-path '("~/bin/emacs")))

;; Backup files
(setq
   backup-by-copying t      ; don't clobber symlinks
   backup-directory-alist
    '(("." . "~/.saves/"))    ; don't litter my fs tree
   delete-old-versions t
   kept-new-versions 8
   kept-old-versions 8
   version-control t)       ; use versioned backups

;; (require 'exwm)
;; (require 'exwm-config)
;; (require 'exwm-systemtray)
;; (exwm-config-default)
;; (exwm-systemtray-enable)

;; Læs andre init filer
(require 'config-gui)
(require 'config-irc)
(require 'config-editor)
(require 'config-helm)
(require 'config-arch)
(require 'config-calender)
(require 'config-cl)
(require 'config-fsharp)
(require 'config-c)
(require 'config-rust)
;;(require 'config-web)
(require 'config-rss)
(require 'config-eshell)
(require 'config-mail)
(require 'config-org)
(require 'config-lisp)

;; enable fasto-mode
(require 'fasto-mode)

;; Discord rich precence
(defun start-elcord ()
  (require 'dbus)
  (defun nm-is-connected()
    (equal 70 (dbus-get-property
               :system "org.freedesktop.NetworkManager" "/org/freedesktop/NetworkManager"
               "org.freedesktop.NetworkManager" "State")))
  (require 'elcord)
  (when (nm-is-connected)
    (elcord-connect))
  (setq elcord-join-and-spectate t)
  )

;; Pakker
(use-package magit
  :ensure t)
(use-package fireplace
  :ensure t)

;; SSH AGENT ;; added --systemd to keychain so may not be needed anymore.
(use-package exec-path-from-shell
  :ensure t)
(require 'exec-path-from-shell)
(exec-path-from-shell-copy-env "SSH_AGENT_PID")
(exec-path-from-shell-copy-env "SSH_AUTH_SOCK")

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(blink-cursor-mode nil)
 '(column-number-mode t)
 '(custom-safe-themes
   '("75c5c39809c52d48cb9dcbf1694bf2d27d5f6fd053777c194e0b69d8e49031c0" "0055e55e6a357a941027139152a67f93376616d3501055d06852f10fdc16bac0" "05eeb814f74b2fd4f2e6e37b4d604eb9b1daaaedfa5e692f1d485250c6b553eb" "54e08527b4f4b127ebf7359acbbbecfab55152da01716c4809682eb71937fd33" "73bff6f2ef60f8a1238a9ca666235d258e3acdeeed85d092ca532788dd7a33c4" default))
 '(magit-commit-arguments '("--gpg-sign=95635FBECE62D446"))
 '(org-enforce-todo-dependencies t)
 '(org-export-with-todo-keywords nil)
 '(org-modules
   '(org-bbdb org-bibtex org-docview org-gnus org-info org-irc org-mhe))
 '(package-selected-packages
   '(selectric-mode forge j-mode php-mode glsl-mode multiple-cursors yaml-mode csv-mode org-bullets org-ref stumpwm-mode circe lua-mode elfeed-goodies elfeed mips-mode emojify moe-theme md4rd gnu-apl-mode racer lsp-rust lsp-flycheck lsp-mode cargo auto-org-md wanderlust exec-path-from-shell fireplace magit cdlatex rust-mode fsharp-mode slime aurel rainbow-delimiters erc-image erc-twitch erc-colorize ercn ranger gruvbox-theme pdf-tools hlinum use-package))
 '(send-mail-function 'smtpmail-send-it)
 '(show-paren-mode t)
 '(smtpmail-smtp-server "asmtp.unoeuro.com")
 '(smtpmail-smtp-service 25)
 '(tool-bar-mode nil))
