;; ERC pakker
(use-package dash
  :ensure t)

(require 'erc-join)
(use-package erc
  :ensure t
  :config
  (setq erc-autojoin-channels-alist '(("freenode.net" "#Damnedkids")
                                      ("irc.snoonet.org" "#yogscast")))
  (erc-autojoin-mode t))
(use-package ercn
  :ensure t)
(use-package erc-colorize
  :ensure t)
(use-package erc-twitch
  :ensure t)
(use-package erc-image
  :ensure t
  :config
  (add-to-list 'erc-modules 'image))
(require 'erc)
;; Notifications
(use-package notifications
  :ensure t)


;; ERC
(erc-update-modules)

(setq erc-log-channels-directory "~/.erc/logs/")
(add-hook 'erc-insert-post-hook 'erc-save-buffer-in-logs)

(setq ercn-notify-rules '((message . all))
      ercn-suppress-rules nil)

(defun do-notify (nickname message)
  (notifications-notify
   :title nickname
   :body message
   ;:app-icon "~/.emacs.d/images/erc.png"
   :urgency 'low))
  
(add-hook 'ercn-notify-hook 'do-notify)

(require 'erc-twitch)

(erc-twitch-enable)

(provide 'config-irc)
