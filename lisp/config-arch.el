(use-package aurel
  :ensure t
  :init
  (setq aurel-download-directory "~/abs"))

(provide 'config-arch)
