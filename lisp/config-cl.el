(use-package slime
  :ensure t)

;;; The SBCL binary and command-line arguments
(setq inferior-lisp-program "/usr/bin/sbcl --noinform")

(provide 'config-cl)
