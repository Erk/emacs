(use-package rust-mode
  :ensure t)

(setq rust-format-on-save nil)

(getenv "RUST_SRC_PATH")

(use-package racer
  :ensure t)
(use-package company
  :ensure t)

(add-hook 'rust-mode-hook #'racer-mode)
(add-hook 'racer-mode-hook #'eldoc-mode)
(add-hook 'racer-mode-hook #'company-mode)

(require 'rust-mode)
(define-key rust-mode-map (kbd "TAB") #'company-indent-or-complete-common)
(setq company-tooltip-align-annotations t)

;; (use-package lsp-mode
;;   :ensure t)
;; (use-package lsp-rust
;;   :ensure t)

;; (with-eval-after-load 'lsp-mode
;;   (require 'lsp-flycheck)
;;   (setq lsp-rust-rls-command '("rustup" "run" "nightly" "rls"))
;;   (require 'lsp-rust))
;; (require 'lsp-mode)

;; (add-hook 'rust-mode-hook #'lsp-rust-enable)
;; (add-hook 'rust-mode-hook #'flycheck-mode)

(provide 'config-rust)
