;; GUI stuff

(setq inhibit-startup-screen t)
(when (fboundp 'tool-bar-mode)
  (tool-bar-mode -1))

;; Slå menu-bar fra som standard. 
(menu-bar-mode -1)

;; Slå blinkende cursor fra.
(blink-cursor-mode -1)

;; enable column-number-mode
(column-number-mode)
(scroll-bar-mode -1)


;; makes scrolling a bit less jumpy.
(setq mouse-wheel-follow-mouse t ;; scroll window under mouse
      scroll-step 1 ;; keyboard scroll one line at a time
      mouse-wheel-progressive-speed nil ;; don't accelerate scrollin
      mouse-wheel-scroll-amount '(1 ((shift) . 1))) ;; one line at a time

(setq ansi-color-faces-vector
   [default default default italic underline success warning error])


;; Emoji support RET IRRETERENDE
;(use-package emojify
;  :ensure t)
;(add-hook 'after-init-hook #'global-emojify-mode)


;; Skrifttyper.
(setenv "LANG" "da_DK.UTF-8")
;;(set-face-font 'default "Source Code Pro-10") ; 13
(custom-set-faces
 '(default
    (
     (t
      (:family "IBM Plex Mono"
       :foundry "IBM "
       :slant normal
       :weight semi-bold
       :height 120
       :width normal)))))
(set-fontset-font t 'unicode "Symbola-12" nil 'prepend) ; 15

(add-to-list 'default-frame-alist '(width  . 90))
(add-to-list 'default-frame-alist '(height . 40))
;(add-to-list 'default-frame-alist '(font . "Source Code Pro-10")) ; 13


;; Skal måske skrives om til at bruge nlinum
;(use-package hlinum
;  :ensure t
;  :bind (([f9] . linum-mode))
;  :init
;  (hlinum-activate))

;; nlinum mode
;;(use-package nlinum
;;  :ensure t
;;  :bind (([f9] . nlinum-mode))
;;  :config
;;  (setq nlinum-mode -1)
;;  (setq nlinum-highlight-current-line t))

;; PDF mode
(use-package pdf-tools
  :ensure t
  :init
  (pdf-tools-install))

;; Paren
(show-paren-mode t)
(setq show-paren-style 'expression)

;; Theme
;(use-package zenburn-theme
;  :ensure t
;  :init
;  (setq custom-enabled-themes (quote (zenburn)))
;  (setq custom-safe-themes
;   (quote
;    ("4e753673a37c71b07e3026be75dc6af3efbac5ce335f3707b7d6a110ecb636a3" default))))

;;(use-package gruvbox-theme
;;  :ensure t)

;; Moe theme: https://github.com/kuanyui/moe-theme.el
(use-package moe-theme
  :ensure t)
(setq moe-theme-highlight-buffer-id nil)
(moe-dark)
(moe-theme-set-color 'red)

;; Ranger
(use-package ranger
  :ensure t
  :bind (([C-.] . ranger))
  :bind (([C-c r] . ranger))
  :bind (([f5] . ranger)))

(ranger-override-dired-mode)

;; Keybinds
(global-set-key (kbd "<f12>") 'menu-bar-mode)
(global-set-key (kbd "<f9>") 'display-line-numbers-mode)

;; Brug windmove.
(windmove-default-keybindings 'meta)

(provide 'config-gui)
