(use-package helm
  :ensure t
  :bind (("M-x" . helm-M-x)
         ("C-x C-f" . helm-find-files))
  :init
  (helm-mode 1))

;(define-key helm-find-files-map (kbd "<tab>")         'helm-execute-persistent-action)

(setq helm-split-window-in-side-p t)
;(helm-autoresize-mode 1)

(setq helm-M-x-fuzzy-match t)

(global-set-key (kbd "M-y") 'helm-show-kill-ring)

(global-set-key (kbd "C-x b") 'helm-mini)

(setq helm-buffers-fuzzy-matching t
      helm-imenu-fuzzy-match      t
      helm-recentf-fuzzy-match    t)

(add-to-list 'helm-sources-using-default-as-input 'helm-source-man-pages)

(use-package helm-tramp
  :ensure t)

(require 'helm-tramp)

(setq tramp-default-method "ssh")
(defalias 'exit-tramp 'tramp-cleanup-all-buffers)
(define-key global-map (kbd "C-c s") 'helm-tramp)


;; (setq helm-projectile-fuzzy-match nil)
;(require 'helm-projectile)
;(helm-projectile-on)


(provide 'config-helm)
