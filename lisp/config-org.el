(use-package cdlatex
  :ensure t)

(require 'cdlatex)

(use-package org-ref
  :ensure t)

(require 'org-ref)

(use-package org-bullets
  :ensure t)

(require 'org-bullets)

(org-babel-do-load-languages 'org-babel-load-languages
                             '((shell . t)))

(add-hook 'org-mode-hook 'turn-on-org-cdlatex)


(require 'org)
(require 'ob-dot)

(define-key global-map "\C-cl" 'org-store-link)
(define-key global-map "\C-ca" 'org-agenda)
(setq org-log-done t)

(setq org-agenda-files (list "~/org/other.org"
                             "~/org/uni.org" 
                             "~/org/home.org"
                             "~/org/cal.org"))

(setq org-latex-default-figure-position "H")

(setq org-src-fontify-natively t)

(setq org-latex-listings 'minted)
(setq org-latex-minted-options
      '(("frame" "lines") ("linenos=true") ("breaklines=true")))

(add-hook 'org-mode-hook (setq org-latex-pdf-process
      (mapcar
       (lambda (s)
         (replace-regexp-in-string "%latex " "%latex -shell-escape " s))
       org-latex-pdf-process)))

(add-hook 'org-mode-hook 'org-bullets-mode)
(add-hook 'org-mode-hook 'auto-fill-mode)
(add-hook 'org-mode-hook 'org-indent-mode)

(provide 'config-org)
