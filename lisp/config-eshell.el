(require 'helm)

(add-hook 'eshell-mode-hook
          (lambda ()
              ;; Helm completion with pcomplete
              (setq eshell-cmpl-ignore-case t)
              (eshell-cmpl-initialize)
              (define-key eshell-mode-map
                [tab]
                'helm-esh-pcomplete)
              ;; Helm completion on eshell history.
              (define-key eshell-mode-map (kbd "M-p") 'helm-eshell-history)))

(provide 'config-eshell)
