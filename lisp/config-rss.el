;; RSS
(use-package elfeed
  :ensure t
  :init
  (setq elfeed-feeds
      '(("https://www.youtube.com/feeds/videos.xml?channel_id=UCH-_hzb2ILSCo9ftVSnrCIQ" Youtube)
        ("https://www.youtube.com/feeds/videos.xml?channel_id=UCxt9Pvye-9x_AIcb1UtmF1Q" Youtube)
        ("http://feeds.reuters.com/Reuters/worldNews" News)))
  :bind (("C-x w" . elfeed)))

(use-package elfeed-goodies
  :ensure t
  :config
  (setq elfeed-goodies/tag-column-width 12))

;;(elfeed-goodies/setup)

(provide 'config-rss)
