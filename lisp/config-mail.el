(setq user-mail-address "valdemar@erk.io")

(require 'smtpmail)

(setq message-send-mail-function 'smtpmail-send-it
     starttls-use-gnutls t
     smtpmail-starttls-credentials
     '(("asmtp.unoeuro.com" 587 nil nil))
     smtpmail-auth-credentials
     (expand-file-name "~/.authinfo.gpg")
     smtpmail-default-smtp-server "asmtp.unoeuro.com"
     smtpmail-smtp-server "asmtp.unoeuro.com"
     smtpmail-smtp-service 587
     smtpmail-debug-info t)
;;Wanderlust
(require 'wl)
(autoload 'wl "wl" "Wanderlust" t)
(autoload 'wl-other-frame "wl" "Wanderlust on new frame." t)
(autoload 'wl-draft "wl-draft" "Write draft with Wanderlust." t)

;; Open ~/.wl in emacs lisp mode.
(add-to-list 'auto-mode-alist '("\.wl$" . emacs-lisp-mode))

;(elmo-passwd-alist-save)
;(use-package w3m
;  :ensure t)
;(setq mm-w3m-safe-url-regexp nil)

;(wl)

(provide 'config-mail)
