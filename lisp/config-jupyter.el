(use-package ein
  :ensure t)

(require 'ein)
(require 'ein-loaddefs)
(require 'ein-notebook)
(require 'ein-subpackages)



(provide 'config-jupyter)
