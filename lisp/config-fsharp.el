
(use-package fsharp-mode
  :ensure t)

(setq-default fsharp-indent-offset 2)

(provide 'config-fsharp)
